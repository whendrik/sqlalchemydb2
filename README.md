# SQLalchemy and DB2 example

## 1. - with `ibm_db`

```
from ibm_db import connect, exec_immediate, fetch_both


conn = connect("DRIVER={{IBM DB2 ODBC DRIVER}};"
                     'DATABASE={database};'
                     'HOSTNAME={host};'
                     'PORT={port};'
                     'PROTOCOL=TCPIP;'
                     'UID={username};'
                     'PWD={password};Security=SSL;'.format(**credentials), '', '')
```

and

```
sql = "SELECT * FROM transactions"

stmt = exec_immediate(conn, sql)
dictionary = fetch_both(stmt)

count = 0

while dictionary != False and count < 10:
    print(dictionary.keys())
    dictionary = fetch_both(stmt)
    
    print(dictionary[0])
    count += 1
```

### Alternative `ibm_db`, with use of with `%%SQL`

https://github.com/DB2-Samples/db2jupyter/blob/master/db2.ipynb


## 2. `jaydebeapi` (support read to Panda's DF)

See notebook

## 3. `sqlalchemy` with `ibm_db_sa`

See notebook

Minimum working example based on

https://dataplatform.cloud.ibm.com/analytics/notebooks/v2/a972effc-394f-4825-af91-874cb165dcfc/view?access_token=ee2bd90bee679afc278cdb23453946a3922c454a6a7037e4bd3c4b0f90eb0924
to get SQLAlchemy configured with DB2, and play around with pandas 
Dataframes

(!) For SSL with SQL Alchemy use

```
DB2_URI= "ibm_db_sa://{username}:{password}@{host}:{port}/{db}?ssl=true".format(**db2_credentials)
```

See https://github.com/ibmdb/python-ibmdbsa/issues/70
